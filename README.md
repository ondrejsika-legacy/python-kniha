# Python kniha

Ondrej Sika <ondrej@ondrejsika.com>

Homepage na <http://ondrejsika.com/books/python-kniha>

Repozitar na <https://github.com/ondrejsika/python-kniha>

Actual build: <https://drive.ondrejsika.com/pdf/python-kniha/_build/Ondrej_Sika__Python_kniha.pdf>

Build with [texb](http://ondrejsika.com/projects/texb.html): `texb`

Pripominky mi prosim davejte do Github issues <https://github.com/ondrejsika/python-kniha/issues> (preferuji) a nebo mi je posilejte emailem <ondrej@ondrejsika.com>

